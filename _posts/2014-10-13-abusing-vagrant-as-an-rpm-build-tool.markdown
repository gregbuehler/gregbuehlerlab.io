---
layout: post
title: Abusing Vagrant as an RPM build tool
date:   2014-10-13 15:32:14 -0300
tags: [vagrant, rpm, ci]
fullview: true
comments: true
---

Lately I've had to (re)package several programs. Thankfully I've been able to move fast by abusing Vagrant to build my rpm's.

My Vagrantfile looks like this:
{% highlight ruby %}
# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|  
  config.vm.box = ENV['BUILD_FOR'] ||= "centos-6.4-base"
  config.vm.provision "shell", path: "tools.sh"
  config.vm.provision "shell", path: "build.sh"
end  
{% endhighlight %}

The tools script makes sure that that we have the RPM toolchain in place for that package and looks something like this:
{% highlight bash %}
#!/bin/bash
sudo yum install -y rpmdevtools rpm-devel rpm-build mock
{% endhighlight %}

While the build script is the actual commands for building the RPM:
{% highlight bash %}
#!/bin/bash
set -x  
set -e

cd /vagrant

# make sure target dir exists
TARGETDIR=`pwd`/target  
if [ ! -d "$TARGETDIR" ]; then  
  mkdir -p $TARGETDIR
fi

# setup rpm build tree
rpmdev-setuptree

# parse file and retreive remote sources
spectool -g foo.spec

# build a source rpm, then build the srpm using mock
RPMBUILD=`rpmbuild -bs --nodeps --define "_sourcedir ." --define "_srcrpmdir ." foo.spec`  
SRPM=`echo $RPMBUILD| awk '{print $2}'`  
sudo mock --clean --resultdir=$TARGETDIR --rebuild `pwd`/$SRPM  
{% endhighlight %}

This lets me build for my base Centos environment, or build for a handful of unicorn environments. The only thing that changes is the box image the RPM is built in.
{% highlight bash %}
$ vagrant up
$ BUILD_FOR=fizz vagrant up
$ BUILD_FOR=buzz vagrant up
{% endhighlight %}
